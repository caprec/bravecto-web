
var logoImageBase64 = ""; 
var logoImageFileExtension = ""; 
var logoImageBase64_2 = ""; 
var logoImageFileExtension_2 = ""; 
var spinner;
var myLocation = "";

$(document).ready(function(){ 
  spinner = new jQuerySpinner({
        parentId: 'page-top'
      });
    $("#userOTP").show(); 
    $("#validateUser").hide();
    $("#userReg").hide(); 
    $("#vetReg").hide();
    var regObj = []; 
//var urlString = "https://dployaltyapi20.azurewebsites.net/api"; 
var urlString2 = "https://dployaltyapi20prod.azurewebsites.net/api";  
var urlString = "https://api.resit.co.za/api";
//NOVA URLs
// var urlString = "https://apim-nova-uat.azure-api.net/loyalty/api";
// var urlString = "https://apim-nova-dev.azure-api.net/loyalty/api";
  
    if(localStorage.getItem('rememberMe') == 'true'){
      var tempObj = localStorage.getItem('loginUser');
      $('#loginEmail').val(tempObj.emailAddress);   
      $('#rememberMe').prop('checked', true); 
    }else{
      $('#loginEmail').attr("autocomplete", "off");
      setTimeout('$("#loginEmail").val("");', 500);
      $('#loginPassword').attr("autocomplete", "off");
      setTimeout('$("#loginPassword").val("");', 500);
 
      $('#rememberMe').prop('checked', false); 
    }
   
    if (location.pathname.split('/').slice(-1)[0] == "register.html") {
        //Country
        $.ajax({
            url: urlString2+'/countries',
            headers: { 
                'Content-Type':'application/json'
            },
            method: 'get',
            dataType: 'json',
            data: JSON.stringify(regObj[0]), 
            success: function(data){  
                 //countryList
                  for(var j=0; j<data.length; j++){ 
                    $('<option value="'+data[j].id+'">'+data[j].name+'</option> ').appendTo('#countryList'); 
                  }  
            },
            error: function (xhr, status, errorThrown) { 
                console.log(status); 
            }
        }); 
        //Provice
        $.ajax({
            url: urlString2+'/provinces',
            headers: { 
                'Content-Type':'application/json'
            },
            method: 'get',
            dataType: 'json',
            data: JSON.stringify(regObj[0]), 
            success: function(data){  
                 //provinceList
                  for(var j=0; j<data.length; j++){ 
                    $('<option value="'+data[j].id+'">'+data[j].name+'</option> ').appendTo('#provinceList'); 
                  }  
            },
            error: function (xhr, status, errorThrown) { 
                console.log(status);
            }
        }); 
        //City
        $.ajax({
            url: urlString2+'/cities',
            headers: { 
                'Content-Type':'application/json'
            },
            method: 'get',
            dataType: 'json',
            data: JSON.stringify(regObj[0]), 
            success: function(data){  
                 //cityList
                  for(var j=0; j<data.length; j++){ 
                    $('<option value="'+data[j].id+'">'+data[j].name+'</option> ').appendTo('#cityList'); 
                  }  
            },
            error: function (xhr, status, errorThrown) { 
                console.log(status); 
            }
        }); 

        //Wholesaler
        $.ajax({
            url: urlString+'/wholesalers',
            headers: { 
                'Content-Type':'application/json'
            },
            method: 'get',
            dataType: 'json',
            data: JSON.stringify(regObj[0]), 
            success: function(data){  
                 //wholesalerList
                  for(var j=0; j<data.length; j++){ 
                    $('<option value="'+data[j].id+'">'+data[j].name+'</option> ').appendTo('#wholeSalerList'); 
                  }  
            },
            error: function (xhr, status, errorThrown) { 
                console.log(status); 
            }
        }); 

        getLocation();
    }

    $("#btnRequestOtp").click(function(){   
        var regVerifyUserEmailAdd = $('#regVerifyUserEmailAdd').val(); 

        if(regVerifyUserEmailAdd.length == 0){
            swal({ 
              title: "Email Address is mandatory", 
              icon: "error",
            });
            return;
        }  

        if (regVerifyUserEmailAdd !== "") {  // If something was entered
            if (!isValidEmailAddress(regVerifyUserEmailAdd)) { 
                swal({ 
                  title: "Email Address is not valid", 
                  icon: "error",
                });
                return;
            }
        }  
           
        spinner.show();

        regObj = []; 
        regObj.push({"emailAddress":regVerifyUserEmailAdd});

        $.ajax({
            url: urlString+"/otps", 
            headers: { 
                'Content-Type':'application/json'
            },
            method: 'POST',
            data: JSON.stringify(regObj[0]), 
            success: function(data){  
              spinner.hide();  
              console.log(data); 
              swal({ 
                  text: "OTP sent to "+regVerifyUserEmailAdd+".", 
                  icon: "success",
                }).then((value) => {
                   $("#userOTP").hide(); 
                   $("#validateUser").show();
                   $("#userReg").hide();
                  $("#vetReg").hide(); 
                  $('#regUserEmailAdd').val($('#regVerifyUserEmailAdd').val());
                });
            },
            error: function (xhr, status, errorThrown) { 
              spinner.hide(); 
                swal({ 
                  title: errorThrown, 
                  icon: "error",
                }); 
            }
        });  
    });

    $("#btnValidateOTP").click(function(){   
        var regVerifyUserEmailAdd = $('#regVerifyUserEmailAdd').val();
        var regValidateOTP = $('#regValidateOTP').val(); 

        if(regValidateOTP.length == 0){
            swal({ 
              title: "OTP is mandatory", 
              icon: "error",
            });
            return;
        }   
           
        spinner.show();

        regObj = []; 
        regObj.push({
            "emailAddress":regVerifyUserEmailAdd,
            "otp":regValidateOTP
        });

        $.ajax({
            url: urlString+'/otps/validate',
            headers: { 
                'Content-Type':'application/json'
            },
            method: 'POST',
            dataType: 'json',
            data: JSON.stringify(regObj[0]), 
            success: function(data){  
              spinner.hide();  
              console.log(data);
               $("#userOTP").hide(); 
               $("#validateUser").hide();
               $("#userReg").show();
              $("#vetReg").hide();
            },
            error: function (xhr, status, errorThrown) { 
              spinner.hide(); 
                swal({ 
                  title: errorThrown, 
                  icon: "error",
                }); 
            }
        }); 
    });

    $("#btnUserReg").click(function(){   
        var regUserName = $('#regUserName').val();
        var regUserLastName = $('#regUserLastName').val();
        var regUserContactNumber = $('#regUserContactNumber').val();
        var regUserEmailAdd = $('#regUserEmailAdd').val();
        var regUserPassword = $('#regUserPassword').val();
        var regUserPassword2 = $('#regUserPassword2').val(); 

        if(regUserName.length == 0){
            swal({ 
              title: "First Name is mandatory", 
              icon: "error",
            });
            return;
        }

        if(regUserLastName.length == 0){
            swal({ 
              title: "Last Name is mandatory", 
              icon: "error",
            });
            return;
        }

        if(regUserContactNumber.length == 0){
            swal({ 
              title: "Contact Number is mandatory", 
              icon: "error",
            });
            return;
        }
 
        if(regUserEmailAdd.length == 0){
            swal({ 
              title: "Email Address is mandatory", 
              icon: "error",
            });
            return;
        }

        if (regUserEmailAdd !== "") {  // If something was entered
            if (!isValidEmailAddress(regUserEmailAdd)) { 
                swal({ 
                  title: "Email Address is not valid", 
                  icon: "error",
                });
                return;
            }
        }

        if(regUserPassword.length == 0){
            swal({ 
              title: "Password is mandatory", 
              icon: "error",
            });
            return;
        }

        if(regUserPassword.length == 0){
            swal({ 
              title: "Password is mandatory", 
              icon: "error",
            });
            return;
        }

        if(regUserPassword2.length == 0){
            swal({ 
              title: "Password repeat is mandatory", 
              icon: "error",
            });
            return;
        }

        if(regUserPassword != regUserPassword2){
            swal({ 
              title: "Passwords do not match", 
              icon: "error",
            });
            return;
        }  
   		$("#userOTP").hide();
           $("#validateUser").hide(); 
   		$("#userReg").hide();
          $("#vetReg").show(); 
       $("#regContainerDIv").addClass("vertical-center2");
    });

    $("#btnVetReg").click(function(){ 
        var regUserName = $('#regUserName').val();
        var regUserLastName = $('#regUserLastName').val();
        var regUserContactNumber = $('#regUserContactNumber').val();
        var regUserEmailAdd = $('#regUserEmailAdd').val();
        var regUserPassword = $('#regUserPassword').val();
        var regUserPassword2 = $('#regUserPassword2').val(); 

    	var inputPracticeName = $('#inputPracticeName').val();
        var inputPracticeVat = $('#inputPracticeVat').val();
        var inputPracticeContactName = $('#inputPracticeContactName').val(); 
        var inputPracticEmail = $('#inputPracticEmail').val();
        var inputPracticeTelephoneNumber = $('#inputPracticeTelephoneNumber').val();
        var inputPracticeStoreAddress = $('#inputPracticeStoreAddress').val();
        var inputPracticeOperatingHours = $('#inputPracticeOperatingHours').val(); 

        var inputPracticeCountryId = $('#countryList').find(":selected").val();
        var inputPracticeProvinceId = $('#provinceList').find(":selected").val();
        var inputPracticeCityId = $('#cityList').find(":selected").val(); 
        var inputWholeSalerID = $('#wholeSalerList').find(":selected").val();
        var suburb = $('#suburb').val();
        var inputPracticeLongitude = 0;
        var inputPracticeLatitude = 0;  

        var regValidateOTP = $('#regValidateOTP').val(); 

        try{
          console.log(myLocation);
          if(myLocation.coords.accuracy > 0){
            inputPracticeLatitude = myLocation.coords.latitude;
            inputPracticeLongitude = myLocation.coords.longitude;
          } 
        }catch (err) {
            console.log(err);
        }

        if(inputPracticeName.length == 0){
            swal({ 
              title: "Practice Name is mandatory", 
              icon: "error",
            });
            return;
        }

        if(inputPracticeVat.length == 0){
            swal({ 
              title: "Vat is mandatory", 
              icon: "error",
            });
            return;
        }

        if(inputPracticeContactName.length == 0){
            swal({ 
              title: "Practice Contact Name is mandatory", 
              icon: "error",
            });
            return;
        }

        if(inputPracticeTelephoneNumber.length == 0){
            swal({ 
              title: "Vet Contact Number is mandatory", 
              icon: "error",
            });
            return;
        }

        if(inputPracticEmail.length == 0){
            swal({ 
              title: "Practice Email Address is mandatory", 
              icon: "error",
            });
            return;
        } 

        if(inputPracticeStoreAddress.length == 0){
            swal({ 
              title: "Practice Address is mandatory", 
              icon: "error",
            });
            return;
        }

        /*if(inputPracticeOperatingHours.length == 0){
            swal({ 
              title: "Practice Operating Hours is mandatory", 
              icon: "error",
            });
            return;
        }*/

        if(inputPracticeCountryId.length == ""){
            swal({ 
              title: "Country is mandatory", 
              icon: "error",
            });
            return;
        }

        if(inputPracticeProvinceId.length == ""){
            swal({ 
              title: "Provice is mandatory", 
              icon: "error",
            });
            return;
        }

        if(inputPracticeCityId.length == ""){
            swal({ 
              title: "City is mandatory", 
              icon: "error",
            });
            return;
        }  

        if(inputWholeSalerID.length == ""){
            swal({ 
              title: "Wholesaler is mandatory", 
              icon: "error",
            });
            return;
        }  

        if (inputPracticEmail !== "") {  // If something was entered
            if (!isValidEmailAddress(inputPracticEmail)) { 
                swal({ 
                  title: "Email Address is not valid", 
                  icon: "error",
                });
                return;
            }
        } 

        if (!$("#Ts_Cs").is(':checked')) {  // If something was entered
            swal({ 
              title: "Please read Ts and Cs before register.", 
              icon: "error",
            });
            return;
        } 
        
        regObj = []; 
        regObj.push({"practiceContactName":inputPracticeContactName,"suburb":suburb,"otp":regValidateOTP,"storeImageBase64":logoImageBase64_2,"storeImageFileExtension":logoImageFileExtension_2,"logoImageBase64":logoImageBase64,"logoImageFileExtension":logoImageFileExtension,"firstName": regUserName, "lastName": regUserLastName, "emailAddress": regUserEmailAdd, "mobileNumber": regUserContactNumber, "password": regUserPassword, "practiceName": inputPracticeName, "practiceContactNumber": inputPracticeTelephoneNumber, "practiceAddress": inputPracticeStoreAddress, "practiceEmail": inputPracticEmail, "practiceVATNumber": inputPracticeVat, "practiceContactOperatingHours": inputPracticeOperatingHours, "countryId": inputPracticeCountryId, "provinceId": inputPracticeProvinceId, "cityId": inputPracticeCityId, "wholesalerId": inputWholeSalerID, "storeGroupId": "8CC7D792-14DD-4740-A587-38DE6D8E6A1F", "longitude": inputPracticeLongitude, "latitude": inputPracticeLatitude}); 
        
        spinner.show();
        $.ajax({
            url: urlString+'/accountrequests',
            headers: { 
                'Content-Type':'application/json'
            },
            method: 'POST',
            dataType: 'json',
            data: JSON.stringify(regObj[0]), 
            success: function(data){
                spinner.hide();  
                logoImageBase64 = "";
                logoImageFileExtension = "";

                logoImageBase64_2 = "";
                logoImageFileExtension_2 = "";
                
                swal({ 
                  title: "Store Created!",
                  text: "Admin will contact you once account has been activated.",
                  icon: "success",
                }).then((value) => {
                  window.location.href = "login.html";
                }); 
            },
            error: function (xhr, status, errorThrown) {  
              spinner.hide(); 
              console.log(xhr);
                swal({ 
                  title: xhr.responseText, 
                  icon: "error",
                }); 
            }
        }); 
    });

    
    $("#rememberMe").click(function(){  
      rememberMe = $("#rememberMe").is(':checked'); 

      if(rememberMe){
        localStorage.setItem('rememberMe', rememberMe); 
      }else{
        localStorage.setItem('rememberMe', rememberMe); 
      }
    });

    $("#Ts_Cs").click(function(){  
      tc = $("#Ts_Cs").is(':checked'); 
      if(tc){ 
        window.open('https://www.msd-animal-health.co.za/bravecto-rewards-portal-terms-conditions/', 'Ts and Cs'); 
      } 
    });

    $("#btnLogin").click(function(){  
    	  var loginEmail = $('#loginEmail').val();
        var loginPassword = $('#loginPassword').val();
        var loginObj = []; 

        if(loginEmail.length == 0){
            swal({ 
              title: "Email is mandatory", 
              icon: "error",
            });
            return;
        }

        if(loginPassword.length == 0){
            swal({ 
              title: "Password is mandatory", 
              icon: "error",
            });
            return;
        }

        loginObj.push({"username": loginEmail,"password": loginPassword}); 
        spinner.show(); 
        $.ajax({
            url: urlString+'/authentication',
            headers: { 
                'Content-Type':'application/json'
            },
            method: 'POST',
            dataType: 'json',
            data: JSON.stringify(loginObj[0]), 
            success: function(data){  
              spinner.hide(); 
            	localStorage.setItem('loginUser', JSON.stringify(data));  
            	window.location.href = "index.html";
            },
            error: function (xhr, status, errorThrown) {  
              spinner.hide(); 
                swal({ 
                  title: xhr.responseText, 
                  icon: "error",
                }); 
            }
        }); 
    });

    $("#btnReset").click(function(){   
        var forgotUser = $('#forgotUser').val(); 
        var forgotUserObj = []; 
        $('#forgotUserNewPassword').val(''); 

        if(forgotUser.length == 0){
            swal({ 
              title: "Email is mandatory", 
              icon: "error",
            });
            return;
        }

        forgotUserObj.push({"emailAddress": forgotUser}); 

        spinner.show();   
        
        $.ajax({
            url: urlString+'/resetpwdrequests',
            headers: { 
                'Content-Type':'application/json'
            },
            method: 'POST',
            dataType: 'json',
            data: JSON.stringify(forgotUserObj[0]), 
            success: function(data){  
              spinner.hide();  
              
              $("#btnReset").hide();

              $("#forgotUserOtp").show();
              $("#forgotUserNewPassword").show();
              $("#forgotUserNewPassword2").show();
              $("#btnResetFinalize").show();
            },
            error: function (xhr, status, errorThrown) { 
              spinner.hide(); 
                swal({ 
                  title: errorThrown, 
                  icon: "error",
                }); 
            }
        }); 
    });

    $("#btnResetFinalize").click(function(){   
        var forgotUser = $('#forgotUser').val(); 
        var forgotUserOtp = $('#forgotUserOtp').val(); 
        var forgotUserNewPassword = $('#forgotUserNewPassword').val(); 
        var forgotUserNewPassword2 = $('#forgotUserNewPassword2').val(); 

        var forgotUserFinalObj = [];  

        if(forgotUser.length == 0){
            swal({ 
              title: "Email is mandatory", 
              icon: "error",
            });
            return;
        }

        if(forgotUserOtp.length == 0){
            swal({ 
              title: "OTP is mandatory", 
              icon: "error",
            });
            return;
        }

        if(forgotUserNewPassword.length == 0){
            swal({ 
              title: "New password is mandatory", 
              icon: "error",
            });
            return;
        }

        if(forgotUserNewPassword2.length == 0){
            swal({ 
              title: "Confirm New password is mandatory", 
              icon: "error",
            });
            return;
        }

        if(forgotUserNewPassword != forgotUserNewPassword2){
            swal({ 
              title: "Passwords do not match", 
              icon: "error",
            });
            return;
        } 
        spinner.show(); 

        forgotUserFinalObj.push({"emailAddress": forgotUser, "otp": forgotUserOtp, "newPwd": forgotUserNewPassword});  
        
        $.ajax({
            url: urlString+'/resetpwdrequests',
            headers: { 
                'Content-Type':'application/json'
            },
            method: 'PUT',
            dataType: 'json',
            data: JSON.stringify(forgotUserFinalObj[0]), 
            success: function(data){  
              spinner.hide(); 
              console.log(data); 
              window.location.href = "index.html";
            },
            error: function (xhr, status, errorThrown) {  
              spinner.hide(); 
                swal({ 
                  title: errorThrown, 
                  icon: "error",
                }); 
            }
        }); 
    });

    
    $("#backBtn").click(function(){ 
        $("#userOTP").hide();
        $("#validateUser").hide();   
        $("#userReg").show();
        $("#vetReg").hide();
        $("#regContainerDIv").removeClass("vertical-center2");
    });
});

function getLocation() {
  if (navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(showPosition);
  } 
}

function showPosition(position) {
  /*x.innerHTML = "Latitude: " + position.coords.latitude + "<br>Longitude: " + position.coords.longitude;*/ 
  myLocation = position;
}

function isValidEmailAddress(emailAddress) {
    var pattern = new RegExp(/^(("[\w-+\s]+")|([\w-+]+(?:\.[\w-+]+)*)|("[\w-+\s]+")([\w-+]+(?:\.[\w-+]+)*))(@((?:[\w-+]+\.)*\w[\w-+]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][\d]\.|1[\d]{2}\.|[\d]{1,2}\.))((25[0-5]|2[0-4][\d]|1[\d]{2}|[\d]{1,2})\.){2}(25[0-5]|2[0-4][\d]|1[\d]{2}|[\d]{1,2})\]?$)/i);
    return pattern.test(emailAddress);
};

function preview() {
  var file = event.target.files[0];
  if (file) {
    if (file.size <= 200000) {
      frame.src = URL.createObjectURL(file); 
      base64($('#file1'), function(data) {
        logoImageBase64 = data.base64;
        getLogoImageFileExtension = data.filetype;
        var parts = getLogoImageFileExtension.split('/', 2);
        logoImageFileExtension = parts[1]; 
      });
    } else {    
      swal({ 
        title:"Please select an image with a size less than or equal to 200 KB.", 
        icon: "error",
      });
      $('#file1').val('');
      frame.src = "img/default.jpg"; 
    }
  }
}

function preview2() {
  var file = event.target.files[0];
  if (file) {
    if (file.size <= 200000) {
      frame2.src = URL.createObjectURL(file);
      base64($('#file2'), function(data) {
        logoImageBase64_2 = data.base64;
        getLogoImageFileExtension = data.filetype;
        var parts = getLogoImageFileExtension.split('/', 2);
        logoImageFileExtension_2 = parts[1];
      });
    } else {
      swal({ 
        title:"Please select an image with a size less than or equal to 200 KB.", 
        icon: "error",
      });
      $('#file2').val('');
      frame2.src = "img/default.jpg";
    }
  }
}



function base64(file, callback){
  var coolFile = {};
  function readerOnload(e){
    var base64 = btoa(e.target.result);
    coolFile.base64 = base64;
    callback(coolFile)
  };

  var reader = new FileReader();
  reader.onload = readerOnload;

  var file = file[0].files[0];
  coolFile.filetype = file.type;
  coolFile.size = file.size;
  coolFile.filename = file.name;
  reader.readAsBinaryString(file);
}

