var productObjUser = [];
var petNamesObjUser = [];
//var urlString = "https://dployaltyapi20.azurewebsites.net/api";
//var urlString = "https://dployaltyapi20prod.azurewebsites.net/api";
var urlString = "https://api.resit.co.za/api";
//NOVA URLs
// var urlString = "https://apim-nova-uat.azure-api.net/loyalty/api";
// var urlString = "https://apim-nova-dev.azure-api.net/loyalty/api";
var authID = "";
var terminalId = "98000002";
var productObj = [];
var userPetObj = [];
var campaignsObj;
var createdPersonID;
var otpResendMsisn;
var selectedId;
var loginUser = JSON.parse(localStorage.getItem("loginUser"));

var logoImageBase64 = "";
var logoImageFileExtension = "";
var changeStoreLogo = false;
var spinner;

$(document).ready(function () {
  spinner = new jQuerySpinner({
    parentId: "page-top",
  });

  $(".controls-top").hide();
  //$(".redeemByCell").hide();
  $("#btnQrCode").hide();
  $(".petInfo").hide();

  // set & get the item in localStorage
  /*localStorage.setItem('test', "hello");
    localStorage.getItem('test');*/

  if (loginUser == null) {
    swal({
      text: "Login is required!",
      icon: "error",
      closeOnClickOutside: true,
    }).then((value) => {
      window.location.href = "login.html";
    });
  } else {
    $("#vetName").text(loginUser.practiceName);
    $("#contactPerson").text(loginUser.practiceContactName);
    $("#contactNumber").text(loginUser.practiceContactNumber);
    $("#contactAdd").text(loginUser.practiceAddress);
    $("#userProfileName").text(loginUser.firstName + " " + loginUser.lastName);

    if (loginUser.practiceLogoImageUrl != null) {
      $("#vetLogo").attr("src", loginUser.practiceLogoImageUrl);
      $("#userProfileLogo").attr("src", loginUser.practiceLogoImageUrl);

      $("#userProfileImg").attr("src", loginUser.practiceLogoImageUrl);
      $("#frame").attr("src", loginUser.practiceLogoImageUrl);
    }

    //user profile
    $("#userProfileFirstName").val(loginUser.firstName);
    $("#userProfileLastname").val(loginUser.lastName);
    $("#userProfileEmailAdd").val(loginUser.emailAddress);
    $("#userProfileMobileNumber").val(loginUser.mobileNumber);

    //practice profile
    $("#practiceNameEdt").val(loginUser.practiceName);
    $("#practiceAddressEdt").val(loginUser.practiceAddress);
    $("#practiceContactNameEdt").val(loginUser.practiceContactName);
    $("#practiceContactNumberEdt").val(loginUser.practiceContactNumber);

    spinner.show();
    authID = "Bearer " + loginUser.token;
    //Get and List all the products.
    $.ajax({
      url: urlString + "/terminals/info",
      headers: {
        Authorization: authID,
        "Content-Type": "application/json",
      },
      method: "GET",
      success: function (data) {
        spinner.hide();
        infoObj = data;
        productObj = data.products;
        campaignsObj = data.campaigns;
        var i;

        var dogObj = [];
        var catObj = [];
        for (var i = 0; i < productObj.length; i++) {
          if (productObj[i].petType == "Dog") {
            dogObj.push(productObj[i]);
          } else {
            catObj.push(productObj[i]);
          }
        }
        productObj = catObj.concat(dogObj);

        dogObj = [];
        catObj = [];
        for (var i = 0; i < campaignsObj.length; i++) {
          if (campaignsObj[i].petType == "Dog") {
            dogObj.push(campaignsObj[i]);
          } else {
            catObj.push(campaignsObj[i]);
          }
        }
        campaignsObj = catObj.concat(dogObj);

        for (var i = 0; i < productObj.length; i++) {
          $(
            '<option value="' +
              productObj[i].id +
              '" id="' +
              productObj[i].id +
              '">' +
              productObj[i].name +
              "</option>"
          ).appendTo("#productSectionDropdown");
        }

        for (var j = 0; j < campaignsObj.length; j++) {
          $(
            '<option value="' +
              campaignsObj[j].id +
              '">' +
              campaignsObj[j].name +
              "</option> "
          ).appendTo("#campaignList");
          $(
            '<option value="' +
              campaignsObj[j].id +
              '">' +
              campaignsObj[j].name +
              "</option> "
          ).appendTo("#campaignList2");
        }
      },
      error: function (xhr, status, errorThrown) {
        spinner.hide();
        swal({
          text: "Login is required!",
          icon: "error",
          closeOnClickOutside: true,
        }).then((value) => {
          window.location.href = "login.html";
        });
      },
    });
    spinner.hide();
  }

  $(".carousel-indicators > li").first().addClass("active");
  $("#carousel").carousel();

  //Product Quantity
  $("#quantityDiv").hide();
  $("#productList").change(function () {
    $("#quantityDiv").show();
  });

  //Add item quantity
  $("#btnAddQty").click(function () {
    var qtyId = $("#productList").find(":selected").val();
    var qty = $("#inputQuantity").val();

    for (var i = 0; i < productObjUser.length; i++) {
      if (productObjUser[i].ProductId == qtyId) {
        productObjUser[i].Quantity = parseInt(qty);
        $("#productList").prop("selectedIndex", 0);
        $("#inputQuantity").val("");
      }
    }
    $("#quantityDiv").hide();
  });

  //Get selected item
  $(document).on("change", "input[id^='quantityDisplay']", function () {
    var index = parseInt($(this).attr("id").replace("quantityDisplay", ""));
    var newQuantity = parseInt($(this).val());
    productObjUser[index].Quantity = newQuantity;
  });

  $("#productSectionDropdown").change(function () {
    var selectedItem = $("#productSectionDropdown").find(":selected").val();
    var alreadySelectedIndex = -1;

    for (var j = 0; j < productObjUser.length; j++) {
      if (selectedItem == productObjUser[j].ProductId) {
        alreadySelectedIndex = j;
        break;
      }
    }

    if (alreadySelectedIndex !== -1) {
      productObjUser[alreadySelectedIndex].Quantity == 1;
      updateQuantityDisplay(
        alreadySelectedIndex,
        productObjUser[alreadySelectedIndex].Quantity
      );
      // Log the item
    } else {
      for (var k = 0; k < productObj.length; k++) {
        if (selectedItem == productObj[k].id) {
          productObjUser.push({ ProductId: productObj[k].id, Quantity: 1 });
          var prodIndex = productObjUser.length - 1;
          var id = productObj[k].id;
          var formContent =
            '<form class="prod"><div ><strong style="text-decoration: underline;">' +
            productObj[k].name +
            '</strong></div><div class="input-group-prepend"><div class=""><label for="qty">Quantity:</label><input class="form-control" type="number" min="1" id="quantityDisplay' +
            prodIndex +
            '" >';

          formContent +=
            '<script>$("#quantityDisplay' + prodIndex + '").val(1);</script>';

          formContent +=
            '<button type="button" class="btn btn-primary btn-user btn-block" style="height: 40px; margin-left: auto; margin-top: 5%;" onclick="deleteProdBtn(' /*id*/ +
            prodIndex +
            ')">Remove</button></div></div></form>';

          $(formContent).appendTo("#productSection");
        }
      }
      $("#productSectionDropdown").attr("disabled", "disabled");
      setTimeout(function () {
        $("#productSectionDropdown").prop("selectedIndex", 0);
        $("#productSectionDropdown").removeAttr("disabled");
      }, 500);
    }
  });

  function updateQuantityDisplay(index, quantity) {
    $("#quantityDisplay" + index).val(quantity);
  }

  $("#inputOrderTotal").on("keydown", function (e) {
    // tab, esc, enter
    if (
      $.inArray(e.keyCode, [9, 27, 13]) !== -1 ||
      // Ctrl+A
      (e.keyCode == 65 && e.ctrlKey === true) ||
      // home, end, left, right, down, up
      (e.keyCode >= 35 && e.keyCode <= 40)
    ) {
      return;
    }

    e.preventDefault();

    // backspace & del
    if ($.inArray(e.keyCode, [8, 46]) !== -1) {
      $(this).val("");
      return;
    }

    var a = ["a", "b", "c", "d", "e", "f", "g", "h", "i", "`"];
    var n = ["1", "2", "3", "4", "5", "6", "7", "8", "9", "0"];

    var value = $(this).val();
    var clean = value.replace(/\./g, "").replace(/,/g, "").replace(/^0+/, "");

    var charCode = String.fromCharCode(e.keyCode);
    var p = $.inArray(charCode, a);
    var q = $.inArray(charCode, n);

    if (p !== -1) {
      value = clean + n[p];

      if (value.length == 2) value = "0" + value;
      if (value.length == 1) value = "00" + value;

      var formatted = "";
      for (var i = 0; i < value.length; i++) {
        var sep = "";
        if (i == 2) sep = ",";
        if (i > 3 && (i + 1) % 3 == 0) sep = ".";
        formatted =
          value.substring(value.length - 1 - i, value.length - i) +
          sep +
          formatted;
      }

      $(this).val(formatted);
    } else if (q !== -1) {
      value = clean + n[q];

      if (value.length == 2) value = "0" + value;
      if (value.length == 1) value = "00" + value;

      var formatted = "";
      for (var i = 0; i < value.length; i++) {
        var sep = "";
        if (i == 2) sep = ",";
        if (i > 3 && (i + 1) % 3 == 0) sep = ".";
        formatted =
          value.substring(value.length - 1 - i, value.length - i) +
          sep +
          formatted;
      }

      $(this).val(formatted);
    }

    return;
  });

  //Earn with Id
  $("#btnEarn").click(function () {
    var orderTotal = $("#inputOrderTotal").val();
    var userID = $("#inputIdUser").val();
    var orderReferenceNo = $("#inputOrderInvoiceNumber").val();
    var earnObj = [];
    var getPetQtyInfo = [];

    if (userID.length == 0) {
      swal({
        title: "Customer cell is required",
        icon: "error",
        closeOnClickOutside: true,
      });
      return;
    }

    if (orderTotal.length == 0) {
      swal({
        title: "Order Total is required",
        icon: "error",
        closeOnClickOutside: true,
      });
      return;
    }

    var tempProductObjUser = [];
    for (var i = 0; i < productObjUser.length; i++) {
      if (productObjUser[i].Quantity > 0) {
        tempProductObjUser.push(productObjUser[i]);
      }
    }

    orderTotal = orderTotal.replace(".", "");
    orderTotal = orderTotal.replace(",", "");

    if (tempProductObjUser.length == 0) {
      swal({
        title: "A product quantity greater than 0 is required",
        icon: "error",
        closeOnClickOutside: true,
      });
      return;
    }

    earnObj.push({
      CustomerMsisdn: userID,
      OrderTotal: parseFloat(orderTotal),
      LineItems: tempProductObjUser,
      orderReferenceNo: orderReferenceNo,
    });
    $("#issueModal").modal("hide");
    spinner.show();

    $.ajax({
      url: urlString + "/orders",
      headers: {
        Authorization: authID,
        "Content-Type": "application/json",
      },
      method: "POST",
      dataType: "json",
      data: JSON.stringify(earnObj[0]),
      success: function (data) {
        console.log(data);
        spinner.hide();
        $("#inputIdUser").val("");
        $("#inputOrderTotal").val("");
        $("#inputOrderInvoiceNumber").val("");
        productObjUser = [];

        $("#productSection").empty();
        $("#issueModal").modal("hide");

        if (data.points > 0) {
          swal({
            text: "Issued stamps successfully.\nStamps earned: " + data.points,
            icon: "success",
            closeOnClickOutside: true,
          });
        } else {
          swal({
            text: "Stamps not issued.\nStamps earned: " + data.points,
            icon: "error",
            closeOnClickOutside: true,
          });
        }
        //
      },
      error: function (xhr, status, errorThrown) {
        spinner.hide();
        $("#inputIdUser").val("");
        $("#inputOrderTotal").val("");
        $("#inputOrderInvoiceNumber").val("");
        productObjUser = [];
    
        $("#productSection").empty();
        $("#issueModal").modal("hide");
        // $("#inputFindCustomer").val(userID);
        // $("#btnFindCustomer").click();
        if (xhr.responseJSON.title == "Not Found") {
          otpfunnction(userID);
        } else {
          swal({
            title: xhr.responseJSON.title,
            icon: "error",
            closeOnClickOutside: true,
           
          });
        }
      }
    
    });
  });

  //Earn without Id
  $("#btnQrCode").click(function () {
    //
    var orderTotal = $("#inputOrderTotal").val();
    var earnObj = [];
    var orderReferenceNo = $("#inputOrderInvoiceNumber").val();

    if (orderTotal.length == 0) {
      swal({
        title: "Order Total is required",
        icon: "error",
        closeOnClickOutside: true,
      });
      return;
    }
    orderTotal = orderTotal.replace(".", "");

    var tempProductObjUser = [];
    for (var i = 0; i < productObjUser.length; i++) {
      if (productObjUser[i].Quantity > 0) {
        tempProductObjUser.push(productObjUser[i]);
      }
    }

    if (tempProductObjUser.length == 0) {
      swal({
        title: "A product quantity greater than 0 is required",
        icon: "error",
        closeOnClickOutside: true,
      });
      return;
    }

    earnObj.push({
      OrderTotal: parseFloat(orderTotal),
      LineItems: tempProductObjUser,
      orderReferenceNo: orderReferenceNo,
    });
    $("#issueModal").modal("hide");
    spinner.show();
    $.ajax({
      url: urlString + "/orders",
      headers: {
        Authorization: authID,
        "Content-Type": "application/json",
      },
      method: "POST",
      dataType: "json",
      data: JSON.stringify(earnObj[0]),
      success: function (data) {
        spinner.hide();
        $("#inputIdUser").val("");
        $("#inputOrderTotal").val("");
        $("#inputOrderInvoiceNumber").val("");
        productObjUser = [];

        $("#productSection").empty();
        $("#issueModal").modal("hide");
        $("canvas").remove();
        $("#qrCodeModal").modal({
          backdrop: "static",
        });
        $("#qrCode").qrcode({
          text: data.pin,
          width: 256,
          height: 256,
        });
        $("#qrCodeNumber").text("Issued Pin: " + data.pin);
      },
      error: function (xhr, status, errorThrown) {
        spinner.hide();
        $("#inputIdUser").val("");
        $("#inputOrderTotal").val("");
        $("#inputOrderInvoiceNumber").val("");
        productObjUser = [];

        $("#productSection").empty();

        //
        $("#issueModal").modal("hide");
        swal({
          title: "QR code error",
          icon: "error",
          closeOnClickOutside: true,
        });
      },
    });
  });

  $("#btnResendOtpTxt").click(function () {
    $.ajax({
      url: urlString + "/people/" + otpResendMsisn + "/otpresends",
      headers: {
        Authorization: authID,
        "Content-Type": "application/json",
      },
      method: "POST",
      success: function (data2) {
        swal({
          text: "OTP Resent",
          icon: "info",
          closeOnClickOutside: true,
        });
      },
      error: function (xhr, status, errorThrown) {
        swal({
          text: errorThrown,
          icon: "error",
          closeOnClickOutside: true,
        });
      },
    });
  });

  //Register
  $("#btnRegister").click(function () {
    //
    $("#btnResendOtpTxt").show();
    var firstName = $("#inputFirstName").val();
    var lastName = $("#inputLastName").val();
    var email = $("#inputEmail").val();
    var cellNumber = $("#inputCellNumber").val();
    var createUserObj = [];

    if (firstName.length == 0) {
      swal({
        title: "First name is required",
        icon: "error",
        closeOnClickOutside: true,
      });
      return;
    }

    if (lastName.length == 0) {
      swal({
        title: "Last name is required",
        icon: "error",
        closeOnClickOutside: true,
      });
      return;
    }

    if (cellNumber.length == 0) {
      swal({
        title: "Cell number is required",
        icon: "error",
        closeOnClickOutside: true,
      });
      return;
    }

    if (email !== "") {
      // If something was entered
      if (!isValidEmailAddress(email)) {
        swal({
          title: "Email Address is not valid",
          icon: "error",
          closeOnClickOutside: true,
        });
        return;
      }
    }

    createUserObj.push({
      FirstName: firstName,
      LastName: lastName,
      Email: email,
      Msisdn: cellNumber,
    });
    $("#regCustomerModal").modal("hide");
    spinner.show();
    $.ajax({
      url: urlString + "/people",
      headers: {
        Authorization: authID,
        "Content-Type": "application/json",
      },
      method: "POST",
      dataType: "json",
      data: JSON.stringify(createUserObj[0]),
      success: function (data) {
        spinner.hide();
        createdPersonID = data.id;
        otpResendMsisn = cellNumber;
        $("#inputFirstName").val("");
        $("#inputLastName").val("");
        $("#inputEmail").val("");
        $("#inputCellNumber").val("");
        //
        $("#regCustomerModal").modal("hide");

        $("#notifyModal").modal({
          backdrop: "static",
        });
      },
      error: function (xhr, status, errorThrown) {
        spinner.hide();
        $("#inputFirstName").val("");
        $("#inputLastName").val("");
        $("#inputEmail").val("");
        $("#inputCellNumber").val("");
        $("#regCustomerModal").modal("hide");
        swal({
          title: xhr.responseText,
          icon: "error",
          closeOnClickOutside: true,
        });
      },
    });
  });

  $("#notifySubmit").click(function () {
    $("#notifyModal").modal("hide");
    $("#otpModal").modal({
      backdrop: "static",
    });
  });

  $("#btnReEnterOtp").click(function () {
    $("#regCustomerModal").modal("hide");
    $("#otpModal").modal({
      backdrop: "static",
    });
  });

  //OTP
  $("#otpSubmit").click(function () {
    //
    var otp = $("#inputOTP").val();
    var createUserObjOTP = [];

    createUserObjOTP.push({ Pin: otp });
    $("#otpModal").modal("hide");
    spinner.show();
    $.ajax({
      url: urlString + "/people/" + createdPersonID + "/pin",
      headers: {
        Authorization: authID,
        "Content-Type": "application/json",
      },
      method: "PUT",
      dataType: "json",
      data: JSON.stringify(createUserObjOTP[0]),
      success: function (data) {
        spinner.hide();
        $("#inputOTP").val("");
        $("#otpModal").modal("hide");
        swal({
          title: "OTP match",
          icon: "success",
          closeOnClickOutside: true,
        });
      },
      error: function (xhr, status, errorThrown) {
        spinner.hide();
        $("#inputOTP").val("");
        $("#otpModal").modal("hide");
        swal({
          title: "OTP doesn't match",
          icon: "error",
          closeOnClickOutside: true,
        });
      },
    });
  });

  //OTP
  $("#otpRedeemSubmit").click(function () {
    //
    var otp = $("#inputOTP2").val();
    var redemptionCode = [];

    redemptionCode.push({ redemptionCode: otp });
    $("#otpRedeemModal").modal("hide");
    spinner.show();
    $.ajax({
      url: urlString + "/redemptions",
      headers: {
        Authorization: authID,
        "Content-Type": "application/json",
      },
      method: "POST",
      dataType: "json",
      data: JSON.stringify(redemptionCode[0]),
      success: function (data) {
        spinner.hide();
        $("#inputOTP2").val("");
        $("#otpRedeemModal").modal("hide");
      },
      error: function (xhr, status, errorThrown) {
        spinner.hide();
        $("#inputOTP2").val("");
        $("#otpRedeemModal").modal("hide");
        swal({
          title: "Redeem code incorrect",
          icon: "error",
          closeOnClickOutside: true,
        });
      },
    });
  });

  // Prepare the preview for profile picture
  $("#wizard-picture").change(function () {
    readURL(this);
  });

  $("#btnRedeem").click(function () {
    //
    var redeemPin = $("#inputRedeemPin").val();
    var cellNum = $("#inputRedeemCell").val();
    var campaignId = $("#campaignList").find(":selected").val();
    var rewardId = $("#rewardList").find(":selected").val();

    var redeemObj = [];

    if (cellNum.length == 0) {
      swal({
        title: "Cell number is required",
        icon: "error",
        closeOnClickOutside: true,
      });
      return;
    }

    if (redeemPin.length == 0) {
      swal({
        title: "Pin is required",
        icon: "error",
        closeOnClickOutside: true,
      });
      return;
    }

    redeemObj.push({ redemptionCode: redeemPin, rewardId: rewardId });
    $("#spendModal").modal("hide");
    spinner.show();
    $.ajax({
      url: urlString + "/redemptions",
      headers: {
        Authorization: authID,
        "Content-Type": "application/json",
      },
      method: "POST",
      dataType: "json",
      data: JSON.stringify(redeemObj[0]),
      success: function (data) {
        spinner.hide();
        $("#inputRedeemCell").val("");
        $("#campaignList").val("");
        $("#rewardList").val("");
        $("#inputRedeemPin").val("");
        $("#spendModal").modal("hide");
        $("#campaignList").prop("selectedIndex", 0);
        $("#rewardList").prop("selectedIndex", 0);

        /*swal({ 
                  text: data.description,
                  icon: "success",
                });*/

        swal({
          text: "Redemption code has been redeemed.",
          icon: "success",
          closeOnClickOutside: true,
        });
      },
      error: function (xhr, status, errorThrown) {
        spinner.hide();
        $("#inputRedeemPin").val("");
        $("#spendModal").modal("hide");
        $("#campaignList").prop("selectedIndex", 0);
        $("#rewardList").prop("selectedIndex", 0);
        swal({
          title: "Redeem code incorrect",
          icon: "error",
          closeOnClickOutside: true,
        });
      },
    });

    /*if(redeemPin.length>0){
            redeemObj.push({"redemptionCode": redeemPin}); 

            $.ajax({
                url: urlString+'/redemptions',
                headers: {
                    'Authorization':authID, 
                    'Content-Type':'application/json'
                },
                method: 'POST',
                dataType: 'json',
                data: JSON.stringify(redeemObj[0]), 
                success: function(data){ 
                    
                    $('#inputRedeemCell').val(''); 
                    $('#campaignList').val('');  
                    $('#rewardList').val('');
                    $('#inputRedeemPin').val('');
                    $('#spendModal').modal('hide'); 
                    $("#campaignList").prop("selectedIndex", 0);
                    $("#rewardList").prop("selectedIndex", 0); 

                    swal({ 
                      text: data.description,
                      icon: "success",
                    });
                },
                error: function (xhr, status, errorThrown) {
                    
                    console.log("error");
                    $('#inputRedeemPin').val(''); 
                    $('#spendModal').modal('hide'); 
                    $("#campaignList").prop("selectedIndex", 0);
                    $("#rewardList").prop("selectedIndex", 0);
                    swal({ 
                      title: "Error try again", 
                      icon: "error",
                    }); 
                }
            });

        }else{
            redeemObj.push({"msisdn": cellNum, "campaignId": campaignId, "rewardId": rewardId});  

            $.ajax({
                url: urlString+'/redeemrequests/terminal',
                headers: {
                    'Authorization':authID, 
                    'Content-Type':'application/json'
                },
                method: 'POST',
                dataType: 'json',
                data: JSON.stringify(redeemObj[0]), 
                success: function(data){ 
                    
                    $('#inputRedeemCell').val(''); 
                    $('#campaignList').val('');  
                    $('#rewardList').val('');
                    $('#inputRedeemPin').val('');
                    $('#spendModal').modal('hide'); 
                    $('#otpRedeemModal').modal({
                    backdrop: 'static'
                });
                },
                error: function (xhr, status, errorThrown) {
                    
                    console.log("error");
                    $('#inputRedeemCell').val(''); 
                    $('#campaignList').val('');  
                    $('#rewardList').val('');
                    $('#inputRedeemPin').val('');
                    $('#spendModal').modal('hide');
                    $('#otpRedeemModal').modal({
                    backdrop: 'static'
                });
                    /*swal({ 
                      title: "Error try again", 
                      icon: "error",
                    }); 
                }
            });
        }*/
  });

  $("#btnRedeemByCell").click(function () {
    //
    var cellNum = $("#inputRedeemCell").val();
    var campaignId = $("#campaignList").find(":selected").val();
    var rewardId = $("#rewardList").find(":selected").val();
    var redeemObj = [];

    redeemObj.push({
      msisdn: cellNum,
      campaignId: campaignId,
      rewardId: rewardId,
    });
    $("#redeemModal").modal("hide");

    spinner.show();
    $.ajax({
      url: urlString + "/redeemrequests/terminal",
      headers: {
        Authorization: authID,
        "Content-Type": "application/json",
      },
      method: "POST",
      dataType: "json",
      data: JSON.stringify(redeemObj[0]),
      success: function (data) {
        spinner.hide();
        $("#inputRedeemCell").val("");
        $("#campaignList").val("");
        $("#rewardList").val("");
        $("#inputRedeemPin").val("");
        $("#redeemModal").modal("hide");
        $("#campaignList").prop("selectedIndex", 0);
        $("#rewardList").prop("selectedIndex", 0);
      },
      error: function (xhr, status, errorThrown) {
        spinner.hide();
        $("#inputRedeemPin").val("");
        $("#redeemModal").modal("hide");
        $("#campaignList").prop("selectedIndex", 0);
        $("#rewardList").prop("selectedIndex", 0);
        swal({
          title: "Redeem was unsuccessful.",
          icon: "error",
          closeOnClickOutside: true,
        });
      },
    });
  });

  $("#btnBalance").click(function () {
    //
    var balanceCell = $("#inputBalanceCell").val();
    var balanceCampID = $("#campaignList2").find(":selected").val();

    if (balanceCell.length == 0) {
      swal({
        title: "Cell number is required",
        icon: "error",
        closeOnClickOutside: true,
      });
      return;
    }
    $("#redeemModal").modal("hide");
    $("#balanceModal").modal("hide");
    spinner.show();
    $.ajax({
      url: urlString + "/stampcards/" + balanceCell + "/" + balanceCampID,
      headers: {
        Authorization: authID,
        "Content-Type": "application/json",
      },
      success: function (data) {
        spinner.hide();
        $("#inputBalanceCell").val("");
        $("#redeemModal").modal("hide");
        $("#balanceModal").modal("hide");
        $("#campaignList2").prop("selectedIndex", 0);

        /*var required = data.numberEarned/data.numberRequired;  
                var required2 = 0;
                try{
                    required2 = data.numberRequired - parseInt(required.toString().split('.')[1].substring(0,1));
                }catch (err) {
                    required2 = data.numberRequired;
                }

                var completed = data.numberEarned/data.numberRequired; 
                var earned = data.numberRequired-required2;*/

        var required = 0;
        var stampsEarned = 0;
        var completed = data.numberEarned / data.numberRequired;

        if (data.numberEarned <= data.numberRequired) {
          required = data.numberRequired - data.numberEarned;
          stampsEarned = data.numberRequired - required;
        } else {
          var temp1 = completed.toString().split(".")[0];
          var temp2 = data.numberRequired * temp1;
          if (data.numberEarned <= temp2) {
            stampsEarned = temp2 - data.numberEarned;
          } else {
            stampsEarned = data.numberEarned - temp2;
          }

          required = data.numberRequired - stampsEarned;
        }

        /*swal({ 
                  title: data.promotionDescription, 
                  text: "Stamps Earned: "+stampsEarned+"\nStamps still required to complete stamp card: "+required+"\nNumber of stamps required to complete stamp card: "+data.numberRequired+"\nCompleted stamp cards available to redeem: "+completed.toString().split('.')[0],
                  icon: "info",
                }); */

        swal({
          title: data.promotionDescription,
          text:
            "Stamps Earned: " +
            stampsEarned +
            "\nStamps still required to complete stamp card: " +
            required +
            "\nCompleted stamp cards available to redeem: " +
            completed.toString().split(".")[0],
          icon: "info",
          closeOnClickOutside: true,
        });
      },
      error: function (xhr, status, errorThrown) {
        spinner.hide();
        $("#inputBalanceCell").val("");
        $("#redeemModal").modal("hide");
        $("#balanceModal").modal("hide");
        $("#campaignList2").prop("selectedIndex", 0);
        swal({
          title: "Balance check failed.",
          icon: "error",
          closeOnClickOutside: true,
        });
      },
    });
  });

  $("#campaignList").change(function () {
    var id = $("#campaignList").find(":selected").val();
    $("#rewardList").empty();
    $("#rewardList").val(null).trigger("change");
    for (var j = 0; j < campaignsObj.length; j++) {
      if (campaignsObj[j].id == id) {
        for (var k = 0; k < campaignsObj[j].rewards.length; k++) {
          $(
            '<option value="' +
              campaignsObj[j].rewards[k].id +
              '">' +
              campaignsObj[j].rewards[k].name +
              "</option> "
          ).appendTo("#rewardList");
        }
      }
    }
  });

  $("#inputRedeemCell").change(function () {
    var stringLength = $("#inputRedeemCell").val();
    /*if(stringLength.length > 0){
            $(".redeemByCell").show();
            $('#inputRedeemPin').val(''); 
        }else{
            $(".redeemByCell").hide();
            $('#inputRedeemCell').val(''); 
        }*/
  });

  $("#inputRedeemPin").change(function () {
    var stringLength = $("#inputRedeemPin").val();
    /*if(stringLength.length > 0){
            $(".redeemByCell").hide();  
            $('#inputRedeemCell').val(''); 
        }else{
            $(".redeemByCell").show();
            $('#inputRedeemPin').val(''); 
        } */
  });

  /*$("#inputIdUser").change(function(){*/
  $("#btnInputIdUser").click(function () {
    var stringLength = $("#inputIdUser").val();

    if (stringLength.length == 0) {
      swal({
        title: "Cell number is required",
        icon: "error",
        closeOnClickOutside: true,
      });
      return;
    }
    //    I need to come here
    $("#petSection").empty();
    if (stringLength.length > 0) {      
      //need api
      $.ajax({
        url: urlString + "/people/" + stringLength + "/petprofile",
        headers: {
          Authorization: authID,
          "Content-Type": "application/json",
        },
        method: "GET",
        success: function (data) {
          userPetObj = data.pets;
          swal({
            text: "Mobile number confirmed",
            icon: "success",
            closeOnClickOutside: true,
          });
          $("#inputOrderTotal").prop("disabled", false);
          $("#productSectionDropdown").prop("disabled", false);
            $("#productSection").show();
            $("#productSectionDropdownDiv").show();
            $(".petInfo").hide();
         
        },
        error: function (xhr, status, errorThrown) {
          $(".petInfo").hide();
          $(".petSection").empty();
          $("#inputOrderTotal").prop("disabled", true);
          $("#productSectionDropdown").prop("disabled", true);
          swal({
            title: stringLength,
            text: "Not registered!!",
            icon: "warning",
            closeOnClickOutside: true,
          });
        },
      });
    } else {
      $("#productSection").show();
      $(".petInfo").hide();
    }
  });
  $("#saveUserProfile").click(function () {
    var userProfileFirstName = $("#userProfileFirstName").val();
    var userProfileLastname = $("#userProfileLastname").val();
    var userProfileEmailAdd = $("#userProfileEmailAdd").val();
    var userProfilePassword = $("#userProfilePassword").val();
    var userProfilePassword2 = $("#userProfilePassword2").val();
    var userProfileMobileNumber = $("#userProfileMobileNumber").val();

    var practiceNameEdt = $("#practiceNameEdt").val();
    var practiceContactNumberEdt = $("#practiceContactNumberEdt").val();
    var practiceAddressEdt = $("#practiceAddressEdt").val();

    var userDetails = [];
    var userDetails2 = [];

    if (userProfileFirstName.length == 0) {
      swal({
        title: "First name is required",
        icon: "error",
        closeOnClickOutside: true,
      });
      return;
    }

    if (userProfileLastname.length == 0) {
      swal({
        title: "Last name is required",
        icon: "error",
        closeOnClickOutside: true,
      });
      return;
    }

    if (userProfileEmailAdd.length == 0) {
      swal({
        title: "Email is required",
        icon: "error",
        closeOnClickOutside: true,
      });
      return;
    }

    if (userProfileMobileNumber.length == 0) {
      swal({
        title: "Mobile Number is required",
        icon: "error",
        closeOnClickOutside: true,
      });
      return;
    }

    // if (userProfilePassword2.length == 0) {
    //   swal({
    //     title: "Repeat password is required",
    //     icon: "error",
    //   });
    //   return;
    // }

    if (userProfilePassword != userProfilePassword2) {
      swal({
        title: "Password are not matching",
        icon: "error",
        closeOnClickOutside: true,
      });
      return;
    }

    if (practiceNameEdt.length == 0) {
      swal({
        title: "Practice name is required",
        icon: "error",
        closeOnClickOutside: true,
      });
      return;
    }

    if (practiceContactNumberEdt.length == 0) {
      swal({
        title: "Practice contact number is required",
        icon: "error",
        closeOnClickOutside: true,
      });
      return;
    }

    if (practiceAddressEdt.length == 0) {
      swal({
        title: "Practice address is required",
        icon: "error",
        closeOnClickOutside: true,
      });
      return;
    }

    spinner.show();
    userDetails.push({
      firstName: userProfileFirstName,
      surname: userProfileLastname,
      emailAddress: userProfileEmailAdd,
      mobileNumber: userProfileMobileNumber,
      // newPassword: userProfilePassword,
      // newPasswordRepeat: userProfilePassword2,
      mustUpdateLogo: changeStoreLogo,
      newLogoBase64: logoImageBase64,
      newLogoFileType: logoImageFileExtension,
    });
    $.ajax({
      url: urlString + "/storeusers/" + loginUser.id,
      headers: {
        Authorization: authID,
        "Content-Type": "application/json",
      },
      method: "PUT",
      dataType: "json",
      data: JSON.stringify(userDetails[0]),
      success: function (data) {
        console.log(data);
        $("#profileModal").modal("hide");
        swal({
          text: "Profile updated!!!",
          icon: "success",
          closeOnClickOutside: true,
        })
        logoImageBase64 = "";
        logoImageFileExtension = "";

        loginUser.emailAddress = data.emailAddress;
        loginUser.firstName = data.firstName;
        loginUser.lastName = data.surname;
        loginUser.practiceLogoImageUrl = data.storeLogoUrl;

        localStorage.setItem("loginUser", JSON.stringify(loginUser));

        userDetails2.push({
          name: practiceNameEdt,
          contactNumber: practiceContactNumberEdt,
          addressLine1: practiceAddressEdt,
        });
        $.ajax({
          url: urlString + "/stores/" + loginUser.storeId,
          headers: {
            Authorization: authID,
            "Content-Type": "application/json",
          },
          method: "PUT",
          dataType: "json",
          data: JSON.stringify(userDetails2[0]),
          success: function (data) {
            loginUser.practiceName = practiceNameEdt;
            loginUser.practiceContactName = practiceContactNumberEdt;
            loginUser.practiceAddress = practiceAddressEdt;
            localStorage.setItem("loginUser", JSON.stringify(loginUser));

            spinner.hide();
            swal({
              text: "Profile updated",
              icon: "success",
              closeOnClickOutside: true,
            }).then((value) => {
              location.reload();
            });
          },
          error: function (xhr, status, errorThrown) {
            spinner.hide();
            swal({
              title: errorThrown,
              icon: "error",
              closeOnClickOutside: true,
            });
          },
        });
      },
      error: function (xhr, status, errorThrown) {
        spinner.hide();
        //user profile
        $("#userProfileFirstName").val(loginUser.firstName);
        $("#userProfileLastname").val(loginUser.lastName);
        $("#userProfileEmailAdd").val(loginUser.emailAddress);

        //practice profile
        $("#practiceNameEdt").val(loginUser.practiceName);
        $("#practiceAddressEdt").val(loginUser.practiceAddress);
        $("#practiceContactNameEdt").val(loginUser.practiceContactName);
        $("#practiceContactNumberEdt").val(loginUser.practiceContactNumber);

        $("#profileModal").modal("hide");
        if (loginUser.practiceLogoImageUrl != null) {
          $("#frame").attr("src", loginUser.practiceLogoImageUrl);
        }
        swal({
          title: errorThrown,
          icon: "error",
          closeOnClickOutside: true,
        });
      },
    });
  });
  // $("#saveUserProfile").click(function () {
  //   var userProfileFirstName = $("#userProfileFirstName").val();
  //   var userProfileLastname = $("#userProfileLastname").val();
  //   var userProfileEmailAdd = $("#userProfileEmailAdd").val();
  //   var userProfilePassword = $("#userProfilePassword").val();
  //   var userProfilePassword2 = $("#userProfilePassword2").val();

  //   var practiceNameEdt = $("#practiceNameEdt").val();
  //   var practiceContactNumberEdt = $("#practiceContactNumberEdt").val();
  //   var practiceAddressEdt = $("#practiceAddressEdt").val();

  //   var userDetails = [];
  //   var userDetails2 = [];

  //   if (userProfileFirstName.length == 0) {
  //     swal({
  //       title: "First name is required",
  //       icon: "error",
  //     });
  //     return;
  //   }

  //   if (userProfileLastname.length == 0) {
  //     swal({
  //       title: "Last name is required",
  //       icon: "error",
  //     });
  //     return;
  //   }

  //   if (userProfileEmailAdd.length == 0) {
  //     swal({
  //       title: "Email is required",
  //       icon: "error",
  //     });
  //     return;
  //   }

  //   if (userProfilePassword.length == 0) {
  //     swal({
  //       title: "Password is required",
  //       icon: "error",
  //     });
  //     return;
  //   }

  //   if (userProfilePassword2.length == 0) {
  //     swal({
  //       title: "Repeat password is required",
  //       icon: "error",
  //     });
  //     return;
  //   }

  //   if (userProfilePassword != userProfilePassword2) {
  //     swal({
  //       title: "Password are not matching",
  //       icon: "error",
  //     });
  //     return;
  //   }

  //   if (practiceNameEdt.length == 0) {
  //     swal({
  //       title: "Practice name is required",
  //       icon: "error",
  //     });
  //     return;
  //   }

  //   if (practiceContactNumberEdt.length == 0) {
  //     swal({
  //       title: "Practice contact number is required",
  //       icon: "error",
  //     });
  //     return;
  //   }

  //   if (practiceAddressEdt.length == 0) {
  //     swal({
  //       title: "Practice address is required",
  //       icon: "error",
  //     });
  //     return;
  //   }

  //   spinner.show();
  //   userDetails.push({
  //     firstName: userProfileFirstName,
  //     surname: userProfileLastname,
  //     emailAddress: userProfileEmailAdd,
  //     newPassword: userProfilePassword,
  //     newPasswordRepeat: userProfilePassword2,
  //     mustUpdateLogo: changeStoreLogo,
  //     newLogoBase64: logoImageBase64,
  //     newLogoFileType: logoImageFileExtension,
  //   });
  //   $.ajax({
  //     url: urlString + "/storeusers/" + loginUser.id,
  //     headers: {
  //       Authorization: authID,
  //       "Content-Type": "application/json",
  //     },
  //     method: "PUT",
  //     dataType: "json",
  //     data: JSON.stringify(userDetails[0]),
  //     success: function (data) {
  //       $("#profileModal").modal("hide");

  //       logoImageBase64 = "";
  //       logoImageFileExtension = "";

  //       loginUser.emailAddress = data.emailAddress;
  //       loginUser.firstName = data.firstName;
  //       loginUser.lastName = data.surname;
  //       loginUser.practiceLogoImageUrl = data.storeLogoUrl;

  //       localStorage.setItem("loginUser", JSON.stringify(loginUser));

  //       userDetails2.push({
  //         name: practiceNameEdt,
  //         contactNumber: practiceContactNumberEdt,
  //         addressLine1: practiceAddressEdt,
  //       });
  //       $.ajax({
  //         url: urlString + "/stores/" + loginUser.storeId,
  //         headers: {
  //           Authorization: authID,
  //           "Content-Type": "application/json",
  //         },
  //         method: "PUT",
  //         dataType: "json",
  //         data: JSON.stringify(userDetails2[0]),
  //         success: function (data) {
  //           loginUser.practiceName = practiceNameEdt;
  //           loginUser.practiceContactName = practiceContactNumberEdt;
  //           loginUser.practiceAddress = practiceAddressEdt;
  //           localStorage.setItem("loginUser", JSON.stringify(loginUser));

  //           spinner.hide();
  //           swal({
  //             text: "Profile updated",
  //             icon: "success",
  //           }).then((value) => {
  //             location.reload();
  //           });
  //         },
  //         error: function (xhr, status, errorThrown) {
  //           spinner.hide();
  //           swal({
  //             title: errorThrown,
  //             icon: "error",
  //           });
  //         },
  //       });
  //     },
  //     error: function (xhr, status, errorThrown) {
  //       spinner.hide();
  //       //user profile
  //       $("#userProfileFirstName").val(loginUser.firstName);
  //       $("#userProfileLastname").val(loginUser.lastName);
  //       $("#userProfileEmailAdd").val(loginUser.emailAddress);

  //       //practice profile
  //       $("#practiceNameEdt").val(loginUser.practiceName);
  //       $("#practiceAddressEdt").val(loginUser.practiceAddress);
  //       $("#practiceContactNameEdt").val(loginUser.practiceContactName);
  //       $("#practiceContactNumberEdt").val(loginUser.practiceContactNumber);

  //       $("#profileModal").modal("hide");
  //       if (loginUser.practiceLogoImageUrl != null) {
  //         $("#frame").attr("src", loginUser.practiceLogoImageUrl);
  //       }
  //       swal({
  //         title: errorThrown,
  //         icon: "error",
  //       });
  //     },
  //   });
  // });
  //============================================== Testing==================================================
  function otpfunnction(userId) {
    $("#btnResendOtpTxt").hide();
    var stringLength = userId;
    if (stringLength.length > 0) {
      spinner.show();
      $.ajax({
        url: urlString + "/people/" + stringLength + "/petprofile",
        headers: {
          Authorization: authID,
          "Content-Type": "application/json",
        },
        method: "GET",
        success: function (data) {
          spinner.hide();
          var dob = data.dateOfBirth;
          if (dob != null) {
            dob = data.dateOfBirth.substring(0, 10);
          }
          var msisdn = userId;
          swal({
            title: "Complete customer registration first to issue stamps",
            text:
              "First Name: " + data.firstName + "\nLast Name: " + data.lastName,
            icon: "info",
            buttons: {
              resendOTP: {
                text: "Resend OTP",
                value: "resendOTP",
                className: "grey-button",
              },
            },
            closeOnClickOutside: true,
          }).then(function (value) {
            if (value === "resendOTP") {
              $.ajax({
                url: urlString + "/people/" + msisdn + "/otpresends",
                headers: {
                  Authorization: authID,
                  "Content-Type": "application/json",
                },
                method: "POST",
                success: function (data2) {
                  createdPersonID = data.id;
                  $("#otpModal").modal({
                    backdrop: "static",
                  });
                },
                error: function (xhr, status, errorThrown) {
                  swal({
                    text: errorThrown,
                    icon: "error",
                  });
                },
              });
            }
          });
        },
        error: function (xhr, status, errorThrown) {
          spinner.hide();
          swal({
            title: "Customer doesn't exist.",
            icon: "error",
          });
        },
      });
    } else {
      swal({
        title: "Customer cell number is required.",
        icon: "error",
      });
    }
  }
  $("#btnFindCustomer").click(function () {
    $("#btnResendOtpTxt").hide();
    var stringLength = $("#inputFindCustomer").val();
    if (stringLength.length > 0) {
        spinner.show();
        $.ajax({
            url: urlString + "/people/" + stringLength + "/petprofile",
            headers: {
                Authorization: authID,
                "Content-Type": "application/json",
            },
            method: "GET",
            success: function (data) {
                spinner.hide();

                var dob = data.dateOfBirth;
                if (dob != null) {
                    dob = data.dateOfBirth.substring(0, 10);
                }
                var msisdn = $("#inputFindCustomer").val();
                swal({
                    title: "Customer Profile Registered",
                    text:
                        "First Name: " +
                        data.firstName +
                        "\nLast Name: " +
                        data.lastName +
                        "\nDate of birth: " +
                        dob,
                    icon: "info",
                    buttons: {
                      resendOTP: {
                        text: "Resend OTP",
                        value: "resendOTP",
                      },
                      ok: {
                        text: "Ok",
                        value: "ok",
                      },
                    },
                    closeOnClickOutside: false, // Prevent automatic close on click outside
                }).then(function (result) {
                    if (result === "resendOTP") {
                        $.ajax({
                            url: urlString + "/people/" + msisdn + "/otpresends",
                            headers: {
                                Authorization: authID,
                                "Content-Type": "application/json",
                            },
                            method: "POST",
                            success: function (data2) {
                                createdPersonID = data.id;
                                $("#otpModal").modal({
                                    backdrop: "static",
                                });
                            },
                            error: function (xhr, status, errorThrown) {
                                swal({
                                    text: errorThrown,
                                    icon: "error",
                                    closeOnClickOutside: true,
                                });
                            },
                        });
                    }
                });

                $("#inputFindCustomer").val("");
            },
            error: function (xhr, status, errorThrown) {
                spinner.hide();
                $("#inputFindCustomer").val("");
                swal({
                    title: "Customer doesn't exist.",
                    icon: "error",
                    closeOnClickOutside: true,
                });
            },
        });
    } else {
        swal({
            title: "Customer cell number is required.",
            icon: "error",
            closeOnClickOutside: true,
        });
    }
});

  

  $("#inputOrderTotal").change(function () {
    var stringLength = $("#inputIdUser").val();
    if (stringLength.length > 0) {
      $("#btnQrCode").hide();
      $("#btnEarn").show();
    } else {
      $("#btnQrCode").show();
      $("#btnEarn").hide();
      $(".petInfo").hide();
    }
  });

  $(".rowDash2").click(function (evt) {
    refresh();
  });

  $(".navbar-nav").click(function (evt) {
    refresh();
  });

  $("#issueWithID").click(function () {
    $("#inputIdUser").show();
    $("#btnInputIdUser").show();
    $("#issueContainer").show();

    $("#inputOrderTotal").prop("disabled", true);
    $("#productSectionDropdown").prop("disabled", true);
    $("#productSectionDropdownLabel").prop("disabled", true);
    $("#productSectionDropdown").empty();
    $('<option value="">--Select--</option>').appendTo(
      "#productSectionDropdown"
    );
    $("#productSectionDropdownDiv").show();
    for (var i = 0; i < productObj.length; i++) {
      $(
        '<option value="' +
          productObj[i].id +
          '" id="' +
          productObj[i].id +
          '">' +
          productObj[i].name +
          "</option>"
      ).appendTo("#productSectionDropdown");
    }

    $("#issueWithID").addClass("active");
    $("#issueWithoutID").removeClass("active");

    $("#btnQrCode").hide();
    $("#btnEarn").show();
    $(".petInfo").hide();

    $("#inputIdUser").val("");
    $("#inputOrderTotal").val("");
    $("#inputOrderInvoiceNumber").val("");
    productObjUser = [];

    $("#productSection").empty();
  });
  $("#issueWithoutID").click(function () {
    $("#inputIdUser").hide();
    $("#btnInputIdUser").hide();
    $("#issueContainer").show();

    $("#inputOrderTotal").prop("disabled", false);
    $("#productSectionDropdown").prop("disabled", false);
    $("#productSectionDropdownLabel").prop("disabled", false);

    $("#productSectionDropdown").empty();
    $('<option value="">--Select--</option>').appendTo(
      "#productSectionDropdown"
    );
    $("#productSectionDropdownDiv").show();
    for (var i = 0; i < productObj.length; i++) {
      $(
        '<option value="' +
          productObj[i].id +
          '" id="' +
          productObj[i].id +
          '">' +
          productObj[i].name +
          "</option>"
      ).appendTo("#productSectionDropdown");
    }

    $("#issueWithID").removeClass("active");
    $("#issueWithoutID").addClass("active");

    $("#btnQrCode").show();
    $("#btnEarn").hide();
    $(".petInfo").hide();

    $("#inputIdUser").val("");
    $("#inputOrderTotal").val("");
    $("#inputOrderInvoiceNumber").val("");
    productObjUser = [];

    $("#productSection").empty();

    $("#productSection").show();
  });

  //Earn without Id
  /*$("#closeQR").click(function(){  
        location.reload();
    });*/
});

function readURL(input) {
  if (input.files && input.files[0]) {
    var reader = new FileReader();

    reader.onload = function (e) {
      $("#wizardPicturePreview").attr("src", e.target.result).fadeIn("slow");
    };
    reader.readAsDataURL(input.files[0]);
  }
}

function getProdQty(id) {
  var tempId = "";
  var tempId = productObjUser[id].ProductId;
  for (var j = 0; j < productObjUser.length; j++) {
    if (tempId == productObjUser[j].ProductId) {
      var qty = $("#" + id).val();
      productObjUser[j].Quantity = parseInt(qty);
    }
  }
}

function deleteProdBtn(index) {
  productObjUser.splice(index, 1);

  $("#productSection").empty();
  let details = "";
  productObjUser.forEach((item, prodIndex) => {
    const desiredObject = productObj.find((obj) => obj.id === item.ProductId);
    details = desiredObject.name;
    name = desiredObject ? desiredObject.name : null;
    var quantity = item.Quantity;

    var formContent =
      '<form class="prod"><div><strong style="text-decoration: underline;">' +
      name +
      '</strong></div><div class="input-group-prepend"><div class=""><label for="qty">Quantity:</label><input class="form-control"type="number" min="1"  value="' +
      quantity +
      '" id="quantityDisplay' +
      prodIndex +
      '">' +
      '<button type="button" class="btn btn-primary btn-user btn-block" style="height: 40px; margin-left: auto; margin-top: 5%;" onclick="deleteProdBtn(' +
      prodIndex +
      ')">Remove</button></div></div></form>';

    $(formContent).appendTo("#productSection");
  });
  swal({
    title: details + " Deleted",
    icon: "success",
    closeOnClickOutside: true,
  });
  $("#productSectionDropdown").attr("disabled", "disabled");
  setTimeout(function () {
    $("#productSectionDropdown").prop("selectedIndex", 0);
    $("#productSectionDropdown").removeAttr("disabled");
  }, 500);
}

function getSelectedPet(index) {
  selectedId = userPetObj[index].id;
  if ($("input[name=" + selectedId + "]").is(":checked")) {
    // checked
    $("#" + selectedId).show();
    $("#" + selectedId + "input").show();
    $("#" + selectedId + "input").val(0);
    $("#" + selectedId + "label").show();
    $("#" + selectedId + "label2").show();
    $("#" + selectedId + "div").show();
    $("<option>--Select--</option>").appendTo("#" + selectedId);
    for (var i = 0; i < productObj.length; i++) {
      $(
        '<option value="' +
          productObj[i].id +
          '">' +
          productObj[i].name +
          "</option>"
      ).appendTo("#" + selectedId);
    }
    //productObjUser.push({"petId": selectedId, "ProductId": "", "Quantity": 0});
  } else {
    // unchecked
    $("#" + selectedId + "div").hide();
    $("#" + selectedId).hide();
    $("#" + selectedId + "input").hide();
    $("#" + selectedId + "label").hide();
    $("#" + selectedId + "label2").hide();
    $("#" + selectedId + "input").val("");
    $("#" + selectedId).empty();

    var tempObj = [];
    for (var i = 0; i < productObjUser.length; i++) {
      if (productObjUser[i].petId != selectedId) {
        tempObj.push(productObjUser[i]);
      }
    }

    productObjUser = tempObj;
  }
}

function refresh() {
  $("#issueContainer").hide();
  $("#issueWithID").removeClass("active");
  $("#issueWithoutID").removeClass("active");
  $.ajax({
    url: urlString + "/terminals/info",
    headers: {
      Authorization: authID,
      "Content-Type": "application/json",
    },
    method: "GET",
    success: function (data) {},
    error: function (xhr, status, errorThrown) {
      swal({
        text: "Session has expired!",
        icon: "error",
        closeOnClickOutside: true,
      }).then((value) => {
        window.location.href = "login.html";
      });
    },
  });
}

function productSectionDropdownPet(id) {
  var petId = userPetObj[id].id;
  var petName = userPetObj[id].name.replace(/ /g, "");
  petName = petName.replace(/[^a-zA-Z0-9]/g, "");
  var selectValue = $("#" + petName + "select")
    .find(":selected")
    .val();

  var existingItemIndex = productObjUser.findIndex(function (item) {
    return item.ProductId === selectValue;
  });

  if (existingItemIndex === -1) {
    productObjUser.push({ petId: petId, ProductId: selectValue, Quantity: 1 });
    petNamesObjUser.push({ petName: petName });

    $("#productSection").show();
    for (var j = 0; j < productObj.length; j++) {
      if (selectValue == productObj[j].id) {
        var prodIndex = productObjUser.length - 1;
        var formContent =
          '<form class="prod"><span>' +
          userPetObj[id].name +
          ':</span><div ><strong style="text-decoration: underline;">' +
          productObj[j].name +
          '</strong></div><div class="input-group-prepend"><!-- <img src="' +
          productObj[j].imageUrl +
          '" class="prodImg"> --!><div class=""><label for="qty">Enter Quantity:</label><input type="number" min="1" value="1" class="form-control quantity-input" id="' +
          prodIndex +
          '" aria-describedby="btnAddQty" required><button type="button" class="btn btn-primary btn-user btn-block" style="height: 40px; margin-left: auto; margin-top: 5%;" onclick="deleteProdBtn(' +
          prodIndex +
          ')">Remove</button></div></div></form>';
        $(formContent).appendTo("#productSection");
      }
    }

    $("#" + petName + "select").prop("selectedIndex", 0);
  }
  $(".quantity-input").on("input", function () {
    var index = parseInt($(this).attr("id"));
    var newQuantity = parseInt($(this).val());
    productObjUser[index].Quantity = newQuantity;
  });
}
function preview() {
  var file = event.target.files[0];

  if (file) {
    if (file.size <= 200000) {
      frame.src = URL.createObjectURL(file);
      changeStoreLogo = true;
      base64($('input[type="file"]'), function (data) {
        logoImageBase64 = data.base64;
        getLogoImageFileExtension = data.filetype;
        var parts = getLogoImageFileExtension.split("/", 2);
        logoImageFileExtension = parts[1];
      });
    } else {
      frame.src = "img/default.jpg";
      $("#file").val("");
      swal({
        title: "Check file size",
        text: "Please select an image with a size less than or equal to 200 KB.",
        icon: "error",
        closeOnClickOutside: true,
      });
    }
  }
}

function isValidEmailAddress(emailAddress) {
  var pattern = new RegExp(
    /^(("[\w-+\s]+")|([\w-+]+(?:\.[\w-+]+)*)|("[\w-+\s]+")([\w-+]+(?:\.[\w-+]+)*))(@((?:[\w-+]+\.)*\w[\w-+]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][\d]\.|1[\d]{2}\.|[\d]{1,2}\.))((25[0-5]|2[0-4][\d]|1[\d]{2}|[\d]{1,2})\.){2}(25[0-5]|2[0-4][\d]|1[\d]{2}|[\d]{1,2})\]?$)/i
  );
  return pattern.test(emailAddress);
}

function base64(file, callback) {
  var coolFile = {};
  function readerOnload(e) {
    var base64 = btoa(e.target.result);
    coolFile.base64 = base64;
    callback(coolFile);
  }

  var reader = new FileReader();
  reader.onload = readerOnload;

  var file = file[0].files[0];
  coolFile.filetype = file.type;
  coolFile.size = file.size;
  coolFile.filename = file.name;
  reader.readAsBinaryString(file);
}
